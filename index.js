var boot = ((Name, global)=>{
    'use strict';

var scriptsLoaded = {
	
};

    

if('undefined'!==typeof module){
	module.exports = boot;
}else if('undefined'!==typeof define){
	define(Name, () => {
		return boot;
	});
}else{
	global[Name]=boot;
}



async function boot(s, cb, doSerial){
 var serial =('undefined'!==typeof doSerial && true === doSerial)?true:false;
 var p =[];
 if('string'===typeof s){
	 s = [s]; 
 }
 
  var size = s.length, i =0;


 for(i = 0; i < size; i++){
	 var l = s[i];
	 var Type = typeof l;
	 if(Type === 'object' && Array.isArray(l)){
		Type = 'array'; 
	 }else if(Type === 'object' && true === l instanceof Promise){
		Type = 'Promise'; 
	 }
   switch(Type){
	   case 'string' :
		if('undefined'!==typeof scriptsLoaded[l]){
			scriptsLoaded[l].requests++;
            p.push(scriptsLoaded[l].p);
		}else{
			
	     scriptsLoaded[l] = {
			 loaded : false,
			 requests : 1,
			 p : new Promise((resolve,reject)=>{
		 var script = window.document.createElement('script');
		 script.onload = ()=>{
			 resolve(script.src);
		 };
		 script.onerror = (e)=>{
			 reject(e);
		 };
		 script.src=l;
		 window.document.head.append(script);
	 }).then((rl)=>{
		 scriptsLoaded[l].loaded = true;
	     return l;
	 })
 };			
			
	        p.push(scriptsLoaded[l].p);
		    if(serial){
				var buf0s = await scriptsLoaded[l].p;
			}
		}
	break;
    //Object: First load values AS dependencies sequential and after that load the key AS final script...
    /*
  {
      "https://myModule" : [
           "https://myDependencyModule" ,
           "https://anythingNeeded" 

      ]
  }

    */
	 case  'object' :	   
		    var seq = [];
		    for(var mdep in l ){
				var subL = l[mdep];
					var sp = boot(subL, ()=>{
						return boot(mdep, null, serial);
					}, serial || Array.isArray(subL));	
				    	
				seq.push(sp);
			}
		   	
		   var objPromise=Promise.all(seq);
		   
		   if(serial){			
					var buf0p = await objPromise;						
		   }

		    p.push(objPromise);
		   break;
	 case 'Promise' :	   
		     p.push(l);
		   break;
	 case  'array' :	   
		     p.push(boot(l, null, true));
		   break;
	   default:
		   var t = typeof l;
		    console.error(`Invalid type: ${t} ${l}`);
		   break;
   }
 }	
	
 var myPromise = Promise.all(p)
	 . then((r)=>{
		  if('function'===typeof cb){
			 return cb(r);
		  }
		 return r;
	  })
	;	

 return myPromise;
}
return boot;
})('@frdl/boot-js', (() => {
  if (typeof self !== 'undefined' && self.self === self &&
    self.Array === Array && self.setInterval === setInterval) {
    return self;
  }
  if (typeof window !== 'undefined' && window.window === window &&
    window.Array === Array && window.setInterval === setInterval) {
    return window;
  }
  if (typeof global !== 'undefined' && global.global === global &&
    global.Array === Array && global.setInterval === setInterval) {
    return global;
  }
  
  return Function('return this')();
})());
